(function () {
    "use strict";

    var PlaceSearch = require("./PlaceSearch.js");
    var TextSearch = require("./TextSearch.js");
    var PlaceDetailsRequest = require("./PlaceDetailsRequest.js");
    var PlaceAutocomplete = require("./PlaceAutocomplete.js");
    var ImageFetch = require("./ImageFetch.js");
    var NearBySearch = require('./NearBySearch.js');
    var GeoLocation = require('./GeoLocation.js');
    module.exports = function (apiKey, outputFormat) {
        return {
            placeSearch: new PlaceSearch(apiKey, outputFormat),
            textSearch: new TextSearch(apiKey, outputFormat),
            placeDetailsRequest: new PlaceDetailsRequest(apiKey, outputFormat),
            placeAutocomplete: new PlaceAutocomplete(apiKey, outputFormat),
            imageFetch: new ImageFetch(apiKey),
            nearBySearch: new NearBySearch(apiKey, outputFormat),
            geoLocation : new GeoLocation(apiKey, outputFormat)
        };
    };

})();

