var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/getNearByLocations', function(req, res, next) {
   "use strict";
    var assert = require("assert");

    var config = require("../config.js");

    var GooglePlaces = require("../lib/placeSearchInterface.js");
    var googleGeoLocation = new GooglePlaces(config.geocodekey, config.outputFormat);
    
    var parameters;
    var latitude;
    var longitude;

    var placeParameters = {
        address: req.body.searchString
    };

   googleGeoLocation.geoLocation(placeParameters, function (error, response) {

      if(response.results == null || response.results == ''){
        res.render('googlePlacesResult', { title: 'Google places Search',data :JSON.stringify('No data'),searchString : JSON.stringify(req.body),isFromErrorPage : 1,latitude : 12.9667, longitude : 77.5667});
      }else{
        
         latitude = parseFloat(response.results[0].geometry.location.lat);
         longitude =parseFloat(response.results[0].geometry.location.lng);
         
         var googlePlaces = new GooglePlaces(config.apiKey, config.outputFormat);     

            /*console.log(latitude+"\n"+longitude);*/
            parameters = {
               location : [latitude,longitude],
               radius : 800,
               query: req.body.placeType
            };
            googlePlaces.textSearch(parameters, function (error, response) {
                if (error) 
                  res.status(500).render('googlePlacesResult', { title: 'No Result', data : 'No result', latitude : latitude , longitude :longitude ,  searchString : JSON.stringify(req.body),isFromErrorPage : 1});
               /* assert.notEqual(response.results.length, 0, "Place search must not return 0 results");*/
                if(req.body.typeOfRequest != "Result Page"){
                   res.render('googlePlacesResult', { title: 'Google places Search', data :JSON.stringify(response.results), latitude : latitude , longitude :longitude ,  searchParameters : JSON.stringify(req.body),isFromErrorPage : 0});
                }else{
                  res.send({ title: 'Google places Search', data :JSON.stringify(response.results), latitude : latitude , longitude :longitude, searchString : JSON.stringify(req.body)});
                }
        
            });
      }
 
    });
   
});

router.post('/getDirection', function(req, res, next) {
 
  console.log(req.body);
  res.render('directionResult',{ title: 'Google direction Search', data :JSON.stringify(req.body)});

});

router.post('/getCurrentLocation', function(req, res, next) {
   "use strict";
    var assert = require("assert");

    var config = require("../config.js");

    var GooglePlaces = require("../lib/placeSearchInterface.js");

    var googleGeoLocation = new GooglePlaces(config.geocodekey, config.outputFormat);
    var latlng = {lat: parseFloat(req.body.latitude), lng: parseFloat(req.body.longitude)};

    googleGeoLocation.geoLocation({'latLng' :latlng }, function (error, response) {
 
    });
});


router.post('/autoComplete', function(req, res, next) {
 
   "use strict";

    var assert = require("assert");

    var PlaceAutocomplete = require("../lib/PlaceAutocomplete.js");
    var config = require("../config.js");

   var autoCompleteInterface = require("../lib/placeSearchInterface.js");
   var autocomplete = new autoCompleteInterface(config.geocodekey, config.outputFormat);
    

    var parameters = {
        input: req.body.searchString
    };

    var outputString;

    autocomplete.placeAutocomplete(parameters, function (error, response) {
        if (error) throw error;
            assert.equal(response.status, "OK", "Place autocomplete request response status is OK");
            outputString = response.results;
        res.send({ title: 'Autocomplete', data :JSON.stringify(response.predictions)});

    });
   

});

router.post('/getPlaceDetails', function(req, res, next) {

  "use strict";

    var assert = require("assert");
 
   console.log(req.body);
   var config = require("../config.js");

    var PlaceDetailsRequest = require("../lib/PlaceDetailsRequest.js");

    var placeDetailsRequest = new PlaceDetailsRequest(config.apiKey, config.outputFormat);

   var placeDetailsInterface = require("../lib/placeSearchInterface.js");
   var placeDetails = new placeDetailsInterface(config.geocodekey, config.outputFormat);
    

    var parameters = {
        placeId : req.body.reference,
    };

   placeDetailsRequest({reference : req.body.reference}, function (error, response) {
        if (error) throw error;
            assert.equal(response.status, "OK", "Place autocomplete request response status is OK");
        console.log(response);
        res.send({ title: 'placeDetails', data :JSON.stringify(response.result)});

    });
});

router.post('/getImages', function(req, res, next) {
 
   "use strict";
  var assert = new require("assert");

  var ImageFetch = require("../lib/ImageFetch.js");
  var config = require("../config.js");

  var imageFetch = new ImageFetch(config.apiKey);

  var parameters = {
    photoreference: req.body.photoReference,
    sensor: false
  };

  imageFetch(parameters, function (error, response) {
    if (error) throw error;
    assert.equal(response, "https://lh4.googleusercontent.com/-5QGMNRIGYO8/UBUFBc1V3yI/AAAAAAAABm4/-CLL9WLEGA0/s1600-w400/DSCN1836.JPG", "the response should be https://lh4.googleusercontent.com/-5QGMNRIGYO8/UBUFBc1V3yI/AAAAAAAABm4/-CLL9WLEGA0/s1600-w400/DSCN1836.JPG");
    res.send({ title: 'Autocomplete', data :JSON.stringify(response)});
  });

});
module.exports = router;
